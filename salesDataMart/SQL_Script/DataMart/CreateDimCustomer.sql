IF NOT EXISTS (SELECT name FROM sys.tables where name = 'DimCustomer')
BEGIN
	CREATE TABLE [dbo].[DimCustomer](
		[CustomerKey] [int] IDENTITY(1,1) NOT NULL,
		[CompanyName] [varchar](55) NOT NULL,
		[Name] [varchar](55) NOT NULL,
		[Number] [numeric](10, 0) NOT NULL,
		[TaxpayerNumber] [varchar](20) NOT NULL,
		[Balance] [numeric](19, 6) NOT NULL,
		[Fax] [varchar](60) NOT NULL,
		[Phone] [varchar](60) NOT NULL,
		[Contact] [varchar](30) NOT NULL,
		[Address] [varchar](55) NOT NULL,
		[ZipCode] [varchar](45) NOT NULL,
		[City] [varchar](45) NULL,
		[Location] [varchar](43) NOT NULL,
		[CustomerType] [varchar](20) NOT NULL,
		[vendedor] [varchar](101) NOT NULL,
		[DueDays] [numeric](3, 0) NOT NULL,
		[Plafond] [numeric](19, 6) NOT NULL,
		[BankAccountNumber] [varchar](28) NOT NULL,
		[Segment] [varchar](50) NULL,
		[Email] [varchar](45) NOT NULL,
		[Mobile] [varchar](45) NOT NULL,
		[IsCurrent] [nvarchar](3),
		[CreationDate] [date] NOT NULL,
		[LastUpdateDate] [date] ,
	 CONSTRAINT [PK_DimCustomer] PRIMARY KEY CLUSTERED 
	(
		[customerKey] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
	) ON [PRIMARY]
	CREATE NONCLUSTERED INDEX [NonClusteredIndex-DimCustomer] ON [dbo].[DimCustomer]
	(
		[Number] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
END
ELSE 
	TRUNCATE TABLE [dbo].[DimCustomer]