IF NOT EXISTS (SELECT name FROM sys.tables where name = 'FactCurrency')
BEGIN
	CREATE TABLE [dbo].[FactCurrency](
		[ConversionDateKey] [int] NOT NULL,
		[SourceCurrencyKey] [int] NOT NULL,
		[DestinationCurrencyKey] [int] NOT NULL,
		[ExchangeRate] [numeric](19, 6) NOT NULL
	 CONSTRAINT [PK_FactCurrency] PRIMARY KEY CLUSTERED 
	(
		[ConversionDateKey] ASC,
		[SourceCurrencyKey] ASC,
		[DestinationCurrencyKey] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
	) ON [PRIMARY]
END
ELSE
	TRUNCATE TABLE [dbo].[FactCurrency]
