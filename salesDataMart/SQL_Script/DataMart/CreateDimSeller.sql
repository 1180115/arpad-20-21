IF NOT EXISTS (SELECT name FROM sys.tables where name = 'DimSeller')
BEGIN
	CREATE TABLE [dbo].[DimSeller](
		[SellerKey] [int] IDENTITY(1,1) NOT NULL,
		[CompanyName] [varchar](55) NOT NULL,
		[Number] [numeric](6, 0) NOT NULL,
		[FullName] [varchar](40) NOT NULL,
		[Address] [varchar](40) NOT NULL,
		[ZipCode] [varchar](10) NOT NULL,
		[City] [varchar](33) NOT NULL,
		[Location] [varchar](43) NOT NULL,
		[Phone] [varchar](60) NOT NULL,
		[Email] [varchar](100) NOT NULL,
		[IsCurrent] [nvarchar](3) NOT NULL,
		[CreationDate] [date] NOT NULL,
		[LastUpdateDate] [date] NULL,
	 CONSTRAINT [PK_DimSeller] PRIMARY KEY CLUSTERED 
	(
		[SellerKey] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
	) ON [PRIMARY]
	CREATE NONCLUSTERED INDEX [NonClusteredIndex-DimSeller] ON [dbo].[DimSeller]
	(
		[Number] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]

END
ELSE
	TRUNCATE TABLE [dbo].[DimSeller]