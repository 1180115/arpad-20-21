IF NOT EXISTS (SELECT name FROM sys.tables where name = 'DimCurrency')
BEGIN
	CREATE TABLE [dbo].[DimCurrency](
		[CurrencyKey] [int] IDENTITY(1,1) NOT NULL,
		[CurrencyName] [nvarchar](100) NOT NULL,
		[CurrencyAbbreviation] [nvarchar](3) NOT NULL,
	PRIMARY KEY CLUSTERED 
	(
		[CurrencyKey] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
	) ON [PRIMARY]
END
ELSE
	TRUNCATE TABLE [dbo].[DimCurrency]

