IF NOT EXISTS (SELECT name FROM sys.tables where name = 'FactSales')
BEGIN
	CREATE TABLE [dbo].[FactSales](
		[DateKey] [int] NOT NULL,
		[ProductKey] [int] NOT NULL,
		[CustomerKey] [int] NOT NULL,
		[EmployeeKey] [int] NOT NULL,
		[SellerKey] [int] ,
		[CurrencyKey] [int] NOT NULL,
		[SupplierKey] [int] NOT NULL,
		[CompanyKey] [int] NOT NULL,

		[SalesNumber] [numeric](10, 0) NOT NULL,
		[DetailsNumber] [varchar](20) NOT NULL,		
		[SaleStamp] [varchar](25) NOT NULL,
		[SaleDetailStamp] [varchar](25) NOT NULL,
		[SaleDetailLineNumber] [numeric](10, 0) NOT NULL,

		[PaymentDateKey] [int] NOT NULL,
		[Quantity] [numeric](14, 0) NOT NULL,
		[VATRate] [numeric](19, 2) NOT NULL,
		[LocalUnitPrice] [numeric](19, 6) NOT NULL,
		[LocalNetSales] [numeric](19, 6) NOT NULL,
		[LocalDiscountAmount] [numeric](19, 6) NOT NULL,
		[LocalCostAmount] [numeric](19, 2) NOT NULL
	) 
END

