IF NOT EXISTS (SELECT name FROM sys.tables where name = 'DimSupplier')
BEGIN
	CREATE TABLE [dbo].[DimSupplier](
		[SupplierKey] [int] IDENTITY(1,1) NOT NULL,
		[CompanyName] [varchar](55) NOT NULL,
		[Number] [numeric](10, 0) NOT NULL,
		[Name] [varchar](55) NOT NULL,
		[Fax] [varchar](60) NOT NULL,
		[Phone] [varchar](60) NOT NULL,
		[Contact] [varchar](30) NOT NULL,
		[Address] [varchar](55) NOT NULL,
		[ZipCode] [varchar](45) NOT NULL,
		[City] [varchar](45) NOT NULL,
		[TaxpayerNumber] [varchar](20) NOT NULL,
		[Type] [varchar](20) NOT NULL,
		[Discount] [numeric](5, 2) NOT NULL,
		[DueDays] [numeric](3, 0) NOT NULL,
		[ContactJob] [varchar](15) NOT NULL,
		[Email] [varchar](45) NOT NULL,
		[Nationality] [char](20) NOT NULL,
		[Mobile] [varchar](45) NOT NULL,
		[Balance] [numeric](19, 6) NOT NULL,
		[Plafond] [numeric](19, 6) NOT NULL,
		[Class] [varchar](50) NOT NULL,
		[IsCurrent] [nvarchar](3) NOT NULL,
		[CreateDate] [date] NOT NULL,
		[LastUpdateDate] [date] NULL,
	 CONSTRAINT [PK_DimSupplier] PRIMARY KEY CLUSTERED 
	(
		[SupplierKey] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
	) ON [PRIMARY]
	CREATE NONCLUSTERED INDEX [NonClusteredIndex-DimSupplier] ON [dbo].[DimSupplier]
	(
		[Number] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]

END
ELSE 
	TRUNCATE TABLE [dbo].[DimSupplier]