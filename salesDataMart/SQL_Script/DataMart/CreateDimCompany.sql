IF NOT EXISTS (SELECT name FROM sys.tables where name = 'DimCompany')
BEGIN
	CREATE TABLE [dbo].[DimCompany](
		[CompanyKey] [int] IDENTITY(1,1) NOT NULL,
		[Name] [varchar](55) NOT NULL,
		[Number] [numeric](10, 0) NOT NULL,
		[TaxpayerNumber] [varchar](20) NOT NULL,
		[Address] [varchar](50) NOT NULL,
		[ZipCode] [varchar](45) NOT NULL,
		[City] [varchar](43) NOT NULL,
		[Phone] [varchar](60) NOT NULL,
		[Fax] [varchar](60) NOT NULL,
		[Contact] [varchar](40) NOT NULL,
		[Job] [varchar](20) NOT NULL,
		[Email] [varchar](45) NOT NULL,
		[Mobile] [varchar](45) NOT NULL,
		[CreationDate] [datetime] NOT NULL,
		[LastUpdateDate] [datetime],
	 CONSTRAINT [PK_DimCompany] PRIMARY KEY NONCLUSTERED 
	(
		[CompanyKey] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
	) ON [PRIMARY]
END
ELSE 
	TRUNCATE TABLE [dbo].[DimCompany]