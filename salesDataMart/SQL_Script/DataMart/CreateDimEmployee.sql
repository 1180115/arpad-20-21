IF NOT EXISTS (SELECT name FROM sys.tables where name = 'DimEmployee')
BEGIN
	CREATE TABLE [dbo].[DimEmployee](
		[EmployeeKey] [int] IDENTITY(1,1) NOT NULL,
		[CompanyName] [varchar](55) NOT NULL,
		[Number] [numeric](6, 0) NOT NULL,
		[Initials] [varchar](3) NOT NULL,
		[Code] [varchar](20) NOT NULL,
		[Name] [varchar](101) NOT NULL,
		[Group] [varchar](20) NOT NULL,
		[Department] [varchar](50) NOT NULL,
		[Email] [varchar](100) NOT NULL,
		[Chief] [varchar](101) NOT NULL,
		[IsCurrent] [nvarchar](3) NOT NULL,
		[CreationDate] [date] NOT NULL,
		[LastUpdateDate] [date] NULL,
	 CONSTRAINT [PK_DimEmployee] PRIMARY KEY CLUSTERED 
	(
		[EmployeeKey] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
	) ON [PRIMARY]
	CREATE NONCLUSTERED INDEX [NonClusteredIndex-DimEmployee] ON [dbo].[DimEmployee]
	(
		[Number] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]

END
ELSE 
	TRUNCATE TABLE [dbo].[DimEmployee]