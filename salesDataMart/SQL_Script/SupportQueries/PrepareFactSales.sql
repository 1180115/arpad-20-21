SELECT 
	entity.Name as CompanyName,
	stradDetails.ProductReference as ProductReference,
	stradDetails.Date as [Date],
	strad.Customer_Number as CustomerNumber,
	strad.EmployeeNumber as EmployeeNumber,
	strad.Seller_Number as SellerNumber,
	strad.Currency as Currency,
	stradProduct.Supplier_Number as SupplierNumber,
	strad.SaleStamp as SaleStamp,
	stradDetails.SaleDetailStamp as SaleDetailStamp,
	strad.PaymentDate as PaymentDate,
	stradDetails.Quantity as Quantity,
	stradDetails.VATRate as VATRate,
	stradDetails.UnitPrice as UnitPrice,
	stradDetails.GrossValue as NetSales,
	stradDetails.DiscountValue as DiscountAmount,
	stradDetails.CostPrice as CostAmount,
	(strad.Freight/stradDetails.Quantity) as AllocatedFreightAmount,
	(stradDetails.GrossValue+ strad.Freight/stradDetails.Quantity)*stradDetails.VATRate/100 as IVAAmount
FROM Project_StagingArea.dbo.StradEntities entity, 
Project_StagingArea.dbo.StradSales strad
inner join Project_StagingArea.dbo.StradSalesDetails stradDetails on strad.SaleStamp = stradDetails.SaleStamp
inner join Project_StagingArea.dbo.StradProducts stradProduct on stradDetails.ProductReference = stradProduct.Reference
;

SELECT 
	entity.Name as CompanyName,
	steinDetails.ProductReference as ProductReference,
	steinDetails.Date as [Date],
	stein.CustomerNumber as CustomerNumber,
	stein.EmployeeNumber as EmployeeNumber,
	stein.SellerNumber as SellerNumber,
	stein.Currency as Currency,
	steinProduct.SupplierNumber as SupplierNumber,
	stein.SaleStamp as SaleStamp,
	steinDetails.SaleDetailStamp as SaleDetailStamp,
	stein.PaymentDate as PaymentDate,
	steinDetails.Quantity as Quantity,
	steinDetails.VATRate as VATRate,
	steinDetails.UnitPrice as UnitPrice,
	steinDetails.GrossValue as NetSales,
	steinDetails.DiscountValue as DiscountAmount,
	steinDetails.CostPrice as CostAmount,
	(stein.Freight/steinDetails.Quantity) as AllocatedFreightAmount,
	(steinDetails.GrossValue+ stein.Freight/steinDetails.Quantity)*steinDetails.VATRate/100 as IVAAmount
FROM Project_StagingArea.dbo.SteinEntities entity, 
Project_StagingArea.dbo.SteinSales stein
inner join Project_StagingArea.dbo.SteinSalesDetails steinDetails on stein.SaleStamp = steinDetails.SaleStamp
inner join Project_StagingArea.dbo.SteinProducts steinProduct on steinDetails.ProductReference = steinProduct.Reference
;


select * from Project_DataMart.dbo.DimCurrency;
select Currency from Stradivarius.dbo.Sales;



select * from Project_DataMart.dbo.FactSales;
--truncate table Project_DataMart.dbo.FactSales;