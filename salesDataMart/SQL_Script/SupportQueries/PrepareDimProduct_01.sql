IF NOT EXISTS (SELECT name FROM sys.tables where name = 'DimProduct')
BEGIN
	CREATE TABLE [dbo].[DimProduct](
		[ProductKey] [int] IDENTITY(1,1) NOT NULL,
		[CompanyName] [varchar](55) NOT NULL,
		[Reference] [char](18) NOT NULL,
		[Description] [char](60) NOT NULL,
		[FamilyName] [char](60) NOT NULL,
		[Stock] [numeric](13, 3) NOT NULL,
		[UnitPrice] [numeric](19, 6) NOT NULL,
		[OrderPoint] [numeric](10, 3) NOT NULL,
		[MinimumStock] [numeric](13, 3) NOT NULL,
		[StartSellingDate] [date] NOT NULL,
		[Category] [varchar](50) NOT NULL,
		[Classification] [varchar](25) NOT NULL,
		[CostPrice] [numeric](19, 6) NOT NULL,
		[CaptiveQuantity] [numeric](13, 3) NOT NULL,
		[VATRate] [numeric](2, 0) NOT NULL,
		[IsCurrent] [nvarchar](3) NOT NULL,
		[CreationDate] [date] NOT NULL,
		[LastUpdateDate] [date] NOT NULL
	 CONSTRAINT [PK_DimProduct] PRIMARY KEY CLUSTERED 
	(
		[ProductKey] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
	) ON [PRIMARY]
END
ELSE
	TRUNCATE TABLE [dbo].[DimProduct]