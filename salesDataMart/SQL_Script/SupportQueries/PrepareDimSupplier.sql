SELECT 
	entity.Name as CompanyName,
	strad.Supplier_Number as Number,
	strad.Supplier_Name as [Name],
	strad.Supplier_Fax as Fax,
	strad.Supplier_Phone as Phone,
	strad.Supplier_Contact as Contact,
	strad.Supplier_Address as [Address],
	strad.Supplier_ZipCode as ZipCode,
	'' as City,
	strad.Supplier_TaxpayerNumber as TaxpayerNumber,
	strad.Supplier_Type as [Type],
	strad.Supplier_Discount as Discount,
	strad.Supplier_DueDays as DueDays,
	strad.Supplier_ContactJob as ContactJob,
	strad.Supplier_Email as Email,
	strad.Supplier_Nationality as Nationality,
	strad.Supplier_Mobile as Mobile,
	strad.Supplier_Balance as Balance,
	strad.Supplier_Plafond as Plafond,
	strad.Supplier_Class as Class,
	strad.CreationDate as CreationDate,
	strad.LastUpdateDate as LastUpdateDate
FROM Project_StagingArea.dbo.StradProducts strad,
Project_StagingArea.dbo.StradEntities entity
;


SELECT 
	entity.Name as CompanyName,
	stein.Number as Number,
	stein.Name as [Name],
	stein.Fax as Fax,
	stein.Phone as Phone,
	stein.Contact as Contact,
	stein.Address as [Address],
	stein.ZipCode as ZipCode,
	stein.City as City,
	stein.TaxpayerNumber as TaxpayerNumber,
	stein.Type as [Type],
	stein.Discount as Discount,
	stein.DueDays as DueDays,
	stein.ContactJob as ContactJob,
	stein.Email as Email,
	stein.Nationality as Nationality,
	stein.Mobile as Mobile,
	stein.Balance as Balance,
	stein.Plafond as Plafond,
	SteinSupplierClasses.Class as Class,
	stein.CreateDate as CreationDate,
	stein.LastUpdateDate as LastUpdateDate
FROM Project_StagingArea.dbo.SteinEntities entity, 
Project_StagingArea.dbo.SteinSuppliers stein
inner join Project_StagingArea.dbo.SteinSupplierClasses on stein.Class = SteinSupplierClasses.Code
;


select * from DimSupplier;
--truncate table DimSupplier;