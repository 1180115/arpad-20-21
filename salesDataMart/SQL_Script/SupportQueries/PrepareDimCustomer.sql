Select 
	entity.[Name] as CompanyName,
	strad.Customer_Name as [Name],
	strad.Customer_Number as number,
	strad.Customer_TaxpayerNumber as TaxpayerNumber,
	strad.Customer_Balance as Balance,
	strad.Customer_Fax as Fax,
	strad.Customer_Phone as Phone,
	strad.Customer_Contact as Contact,
	strad.Customer_Address as [Address],
	strad.Customer_ZipCode as ZipCode,
	strad.Customer_ZipCode as City,
	strad.Customer_Location as [Location], 
	'N/A' as vendedor,
	strad.Customer_DueDays as DueDays,
	strad.Customer_Plafond as Plafond,
	strad.Customer_BanckAccountNumber as BankAccountNumber,
	strad.Customer_Segment as Segment,
	strad.Customer_Email as Email,
	strad.Customer_Mobile as Mobile,
	strad.Customer_Type as [Type],
	strad.CreationDate as CreationDate,
	strad.LastUpdateDate as LastUpdateDate
from Project_StagingArea.dbo.StradEntities entity,
Project_StagingArea.dbo.StradSales strad
;

Select 
	entity.Name as CompanyName,
	stein.Name as [Name],
	stein.Number as number,
	stein.TaxpayerNumber as TaxpayerNumber,
	stein.Balance as Balance,
	stein.Fax as Fax,
	stein.Phone as Phone,
	stein.Contact as Contact,
	stein.Address as [Address],
	stein.ZipCode as ZipCode,
	stein.City as City,
	stein.Location as [Location], 
	steinVendedor.Forename as vendedorFirstName,
	steinVendedor.Surname as vendedorLastName,
	stein.DueDays as DueDays,
	stein.Plafond as Plafond,
	stein.BankAccountNumber as BankAccountNumber,
	stein.Segment as Segment,
	stein.Email as Email,
	stein.Mobile as Mobile,
	steinType.Type as [Type],
	stein.CreationDate as CreationDate,
	stein.LastUpdateDate as LastUpdateDate
from Project_StagingArea.dbo.SteinEntities entity,
Project_StagingArea.dbo.SteinCustomers stein
left join Project_StagingArea.dbo.SteinCustomerTypes steinType on stein.CustomerType = steinType.Code
left join Project_StagingArea.dbo.SteinEmployees steinVendedor on stein.vendedor = steinVendedor.Number
;

select * from Project_DataMart.dbo.DimCustomer;
--truncate table Project_DataMart.dbo.DimCompany;

