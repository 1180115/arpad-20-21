SELECT 
	entity.Name as CompanyName,
	strad.Supplier_Number as Number,
	strad.Supplier_Name as [Name],
	strad.Supplier_Fax as Fax,
	strad.Supplier_Phone as Phone,
	strad.Supplier_Contact as Contact,
	strad.Supplier_Address as [Address],
	strad.Supplier_ZipCode as ZipCode,
	'' as City,
	strad.Supplier_TaxpayerNumber as TaxpayerNumber,
	strad.Supplier_Type as [Type],
	strad.Supplier_Discount as Discount,
	strad.Supplier_DueDays as DueDays,
	strad.Supplier_ContactJob as ContactJob,
	strad.Supplier_Email as Email,
	strad.Supplier_Nationality as Nationality,
	strad.Supplier_Mobile as Mobile,
	strad.Supplier_Balance as Balance,
	strad.Supplier_Plafond as Plafond,
	strad.CreationDate as CreationDate,
	strad.LastUpdateDate as LastUpdateDate
FROM Project_StagingArea.dbo.StradProducts strad,
Project_StagingArea.dbo.StradEntities entity
;


SELECT 
	entity.Name as CompanyName,
	stein.Reference as Reference,
	stein.Description as [Description],
	SteinFamilies.Name as [FamilyName],
	stein.Stock as Stock,
	stein.UnitPrice as UnitPrice,
	stein.OrderPoint as OrderPoint,
	stein.MinimunStock as MinimumStock,
	stein.StartSellingDate as StartSellingDate,
	stein.Category as Category,
	stein.Classification as [Classification],
	stein.CostPrice as CostPrice,
	stein.CaptiveQuantity as CaptiveQuantity,
	stein.VATRate as VATRate,
	stein.CreationDate as CreationDate,
	stein.LastUpdateDate as LastUpdateDate
FROM Project_StagingArea.dbo.SteinEntities entity, 
Project_StagingArea.dbo.SteinProducts stein
inner join Project_StagingArea.dbo.SteinFamilies on stein.FamilyCode = SteinFamilies.Code
;