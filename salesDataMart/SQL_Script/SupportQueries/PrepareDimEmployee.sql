SELECT 
	entity.Name as CompanyName,
	strad.Number as Number,
	strad.Initials as Initials,
	strad.Code as Code,
	strad.Name as [Name],
	strad.[Group] as [Group],
	strad.Department as Department,
	strad.Email as Email,
	--strad.Chief as Chief,
	stradChief.Name as Chief,
	strad.CreationDate as CreationDate,
	strad.LastUpdateDate as LastUpdateDate
FROM Project_StagingArea.dbo.StradEntities entity, 
Project_StagingArea.dbo.StradEmployees strad 
left join Project_StagingArea.dbo.StradEmployees stradChief on strad.Chief = stradChief.Number
;


SELECT 
	entity.Name as CompanyName,
	stein.Number as Number,
	stein.Initials as Initials,
	stein.Code as Code,
	stein.Forename as FirstName,
	stein.Surname as LastName,
	stein.[Group] as [Group],
	SteinDepartments.Department as Department,
	stein.Email as Email,
	--stein.Chief as Chief,
	steinChief.Forename as ChiefFirstName,
	steinChief.Surname as ChiefLastName,
	stein.CreationDate as CreationDate,
	stein.LastUpdateDate as LastUpdateDate
FROM Project_StagingArea.dbo.SteinEntities entity, 
Project_StagingArea.dbo.SteinEmployees stein
inner join Project_StagingArea.dbo.SteinDepartments on stein.Department = SteinDepartments.code
left join Project_StagingArea.dbo.SteinEmployees steinChief on stein.Chief = steinChief.Number
;

select * from Project_DataMart.dbo.DimEmployee;
--truncate table Project_DataMart.dbo.DimEmployee;