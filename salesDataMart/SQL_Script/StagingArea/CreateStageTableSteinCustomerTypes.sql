IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'SteinCustomerTypes')

CREATE TABLE [dbo].[SteinCustomerTypes](
	[Code] [int] NOT NULL,
	[Type] [char](20) NOT NULL

)
ELSE
	TRUNCATE TABLE SteinCustomerTypes

