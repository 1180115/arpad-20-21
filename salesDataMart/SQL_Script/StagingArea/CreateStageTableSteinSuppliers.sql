IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'SteinSuppliers')

CREATE TABLE [dbo].[SteinSuppliers](
	[Number] [numeric](10, 0) NOT NULL,
	[Name] [varchar](55) NOT NULL,
	[Fax] [varchar](60) NOT NULL,
	[Phone] [varchar](60) NOT NULL,
	[Contact] [varchar](30) NOT NULL,
	[Address] [varchar](55) NOT NULL,
	[ZipCode] [varchar](10) NOT NULL,
	[City] [varchar](45) NOT NULL,
	[TaxpayerNumber] [varchar](20) NOT NULL,
	[Type] [varchar](20) NOT NULL,
	[Discount] [numeric](5, 2) NOT NULL,
	[DueDays] [numeric](3, 0) NOT NULL,
	[ContactJob] [varchar](15) NOT NULL,
	[Email] [varchar](45) NOT NULL,
	[Nationality] [char](20) NOT NULL,
	[Mobile] [varchar](45) NOT NULL,
	[Balance] [numeric](19, 6) NOT NULL,
	[Plafond] [numeric](19, 6) NOT NULL,
	[Class] [int] NOT NULL,
	[CreateDate] [date] NOT NULL,
	[LastUpdateDate] [date] NOT NULL

) 

ELSE
	TRUNCATE TABLE SteinSuppliers