IF NOT EXISTS (SELECT name FROM sys.tables where name = 'FactSalesDQP')
BEGIN
	CREATE TABLE [dbo].[FactSalesDQP](
		[CompanyName]  [varchar](55) NOT NULL,
		[ProductReference]  [varchar](18) NOT NULL,
		[Date] [date] NOT NULL,
		[CustomerNumber] [numeric](10, 0) NOT NULL,
		[EmployeeNumber] [numeric](6, 0) NOT NULL,
		[SellerNumber] [numeric](4, 0) NOT NULL,
		[Currency] [varchar](20) NOT NULL,
		[SupplierNumber] [numeric](10, 0) NOT NULL,

		[SaleStamp] [varchar](25) NOT NULL,
		[SaleDetailStamp] [varchar](25) NOT NULL,

		[PaymentDate] [date] NOT NULL,
		[Quantity] [numeric](14, 0) NOT NULL,
		[VATRate] [numeric](19, 2) NOT NULL,
		[UnitPrice] [numeric](19, 6) NOT NULL,
		[NetSales] [numeric](19, 6) NOT NULL,
		[DiscountAmount] [numeric](19, 6) NOT NULL,
		[CostAmount] [numeric](19, 2) NOT NULL,
		[AllocatedFreightAmount] [numeric](38, 21) NOT NULL,
		[IVAAmount] [numeric](38, 6) NOT NULL,
		[DQP] [nvarchar](100) 



	) 
END
ELSE 
	TRUNCATE TABLE [FactSalesDQP]

