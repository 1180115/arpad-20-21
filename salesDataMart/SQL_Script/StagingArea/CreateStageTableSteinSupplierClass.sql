IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'SteinSupplierClasses')

CREATE TABLE [dbo].[SteinSupplierClasses](
	[Code] [int] NOT NULL,
	[Class] [varchar](50) NOT NULL
) 

ELSE
	TRUNCATE TABLE SteinSupplierClasses