IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'SteinCustomersDQP')

CREATE TABLE [dbo].SteinCustomersDQP(
	[Name] [varchar](55) NOT NULL,
	[Number] [numeric](10, 0) NOT NULL,
	[CustomerType] [int] NOT NULL,
	[DQP] [nvarchar](200) NOT NULL,
)

ELSE
	TRUNCATE TABLE SteinCustomersDQP