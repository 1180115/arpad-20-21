IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'SteinDepartments')

CREATE TABLE [dbo].[SteinDepartments](
	[Code] [int] NOT NULL,
	[Department] [varchar](50) NOT NULL
) 

ELSE
	TRUNCATE TABLE SteinDepartments