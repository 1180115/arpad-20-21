IF NOT EXISTS (SELECT name FROM sys.tables where name = 'FactSales')
BEGIN
	CREATE TABLE [dbo].[FactSales](
		[ProductKey] [int] NOT NULL,
		[DateKey] [int] NOT NULL,
		[SellerKey] [int] NOT NULL,
		[SupplierKey] [int] NOT NULL,
		[CustomerKey] [int] NOT NULL,
		[CompanyKey] [int] NOT NULL,
		[EmployeeKey] [int] NOT NULL,
		[LocalCurrencyKey] [int] NOT NULL,
		[PaymentDateKey] [int] NOT NULL,
		[DetailsDateKey] [int] NOT NULL,
		[Number] [numeric](10, 0) NOT NULL,
		[ProductsGrossValue] [numeric](19, 6) NOT NULL,
		[DiscountTotal] [numeric](19, 6) NOT NULL,
		[Freight] [numeric](19, 6) NOT NULL,
		[ValueForVATIncidence] [numeric](19, 6) NOT NULL,
		[VATTotal] [numeric](19, 6) NOT NULL,
		[TotalValue] [numeric](19, 6) NOT NULL,
		[TotalCostPrices] [numeric](19, 6) NOT NULL,
		[SaleDetailStamp] [nvarchar](25) NOT NULL,
		[DetailsNumber] [nvarchar](20) NOT NULL,
		[SaleDetailLineNumber] [numeric](10, 0) NOT NULL,
		[SalesQuantity] [numeric](14, 0) NOT NULL,
		[VATRate] [numeric](4, 2) NOT NULL,
		[LocalUnitPrice] [numeric](19, 6) NOT NULL,
		[LocalNetSales] [numeric](19, 6) NOT NULL,
		[LocalDiscountAmount] [numeric](19, 6) NOT NULL,
		[LocalCostAmount] [numeric](19, 2) NOT NULL,
		[LocalFreightAmount] [numeric](19, 6) NOT NULL,
		[LocalIVAAmount] [numeric](19, 6) NOT NULL,
		[LocalSalesProfit] [numeric](19, 6) NOT NULL,
		[LocalSalesAmount] [numeric](19, 6) NOT NULL,
		[StandardUnitPrice] [numeric](19, 6) NOT NULL,
		[StandardNetSales] [numeric](19, 6) NOT NULL,
		[StandardDiscountAmount] [numeric](19, 6) NOT NULL,
		[StandardCostAmount] [numeric](19, 6) NOT NULL,
		[StandardFreightAmount] [numeric](19, 6) NOT NULL,
		[StandardIVAAmount] [numeric](19, 6) NOT NULL,
		[StandardSalesProfit] [numeric](19, 6) NOT NULL,
		[StandadSalesAmount] [numeric](19, 6) NOT NULL,
	 CONSTRAINT [PK_FactSales] PRIMARY KEY CLUSTERED 
	(
		[ProductKey] ASC,
		[SellerKey] ASC,
		[CustomerKey] ASC,
		[CompanyKey] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
	) ON [PRIMARY]
	GO

	ALTER TABLE [dbo].[FactSales]  WITH CHECK ADD  CONSTRAINT [FK_FactSales_DimCompany] FOREIGN KEY([CompanyKey])
	REFERENCES [dbo].[DimCompany] ([CompanyKey])
	GO

	ALTER TABLE [dbo].[FactSales] CHECK CONSTRAINT [FK_FactSales_DimCompany]
	GO

	ALTER TABLE [dbo].[FactSales]  WITH CHECK ADD  CONSTRAINT [FK_FactSales_DimCurrency] FOREIGN KEY([LocalCurrencyKey])
	REFERENCES [dbo].[DimCurrency] ([CurrencyKey])
	GO

	ALTER TABLE [dbo].[FactSales] CHECK CONSTRAINT [FK_FactSales_DimCurrency]
	GO

	ALTER TABLE [dbo].[FactSales]  WITH CHECK ADD  CONSTRAINT [FK_FactSales_DimCustomer] FOREIGN KEY([CustomerKey])
	REFERENCES [dbo].[DimCustomer] ([CustomerKey])
	GO

	ALTER TABLE [dbo].[FactSales] CHECK CONSTRAINT [FK_FactSales_DimCustomer]
	GO

	ALTER TABLE [dbo].[FactSales]  WITH CHECK ADD  CONSTRAINT [FK_FactSales_DimDate] FOREIGN KEY([DateKey])
	REFERENCES [dbo].[DimDate] ([DateKey])
	GO

	ALTER TABLE [dbo].[FactSales] CHECK CONSTRAINT [FK_FactSales_DimDate]
	GO

	ALTER TABLE [dbo].[FactSales]  WITH CHECK ADD  CONSTRAINT [FK_FactSales_DimEmployee] FOREIGN KEY([EmployeeKey])
	REFERENCES [dbo].[DimEmployee] ([ExployeeKey])
	GO

	ALTER TABLE [dbo].[FactSales] CHECK CONSTRAINT [FK_FactSales_DimEmployee]
	GO

	ALTER TABLE [dbo].[FactSales]  WITH CHECK ADD  CONSTRAINT [FK_FactSales_DimProduct] FOREIGN KEY([ProductKey])
	REFERENCES [dbo].[DimProduct] ([ProductKey])
	GO

	ALTER TABLE [dbo].[FactSales] CHECK CONSTRAINT [FK_FactSales_DimProduct]
	GO

	ALTER TABLE [dbo].[FactSales]  WITH CHECK ADD  CONSTRAINT [FK_FactSales_DimSeller] FOREIGN KEY([SellerKey])
	REFERENCES [dbo].[DimSeller] ([SellerKey])
	GO

	ALTER TABLE [dbo].[FactSales] CHECK CONSTRAINT [FK_FactSales_DimSeller]
	GO

	ALTER TABLE [dbo].[FactSales]  WITH CHECK ADD  CONSTRAINT [FK_FactSales_DimSupplier] FOREIGN KEY([SupplierKey])
	REFERENCES [dbo].[DimSupplier] ([SupplierKey])
	GO

	ALTER TABLE [dbo].[FactSales] CHECK CONSTRAINT [FK_FactSales_DimSupplier]
	GO
END

ELSE
	TRUNCATE TABLE [dbo].[FactSales]

